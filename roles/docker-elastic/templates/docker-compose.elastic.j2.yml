---
version: '3.8'

services:

  elasticsearch:
    image: "opensearchproject/opensearch:{{ opensearch_version }}"
    environment:
      - "OPENSEARCH_JAVA_OPTS=-Xms{{ elasticsearch_memory }}m -Xmx{{ elasticsearch_memory }}m -Djdk.tls.disabledAlgorithms=\"{{ elastic_disabled_tls_protocols|join(',') }}\""
      - "cluster.name=${ELASTIC_CLUSTER_NAME}"
      - "DISABLE_INSTALL_DEMO_CONFIG=true"
      - "DISABLE_SECURITY_PLUGIN=true"
    entrypoint: > # todo : remove temporary opensearch plugin installation due to  https://github.com/opensearch-project/anomaly-detection-dashboards-plugin/issues/156
      sh -c '
        cp -f /tmp/readonly/opensearch.yml /usr/share/opensearch/config/opensearch.yml &&
        /usr/share/opensearch/bin/opensearch-plugin remove opensearch-anomaly-detection &&
        /usr/share/opensearch/bin/opensearch-plugin install --batch https://github.com/opensearch-project/anomaly-detection/releases/download/1.2.0.1/opensearch-anomaly-detection-1.2.0.0.zip &&
        /usr/share/opensearch/opensearch-docker-entrypoint.sh 
      '
    configs:
      - source: opensearch.yml
        target: /tmp/readonly/opensearch.yml
    ulimits:
      memlock:
        soft: -1
        hard: -1
      nofile:
        soft: 65536 # maximum number of open files for the OpenSearch user, set to at least 65536 on modern systems
        hard: 65536
    deploy:
      mode: replicated
      replicas: 1
      placement:
        constraints:
          - node.labels.elasticsearch_host == true
        preferences:
          - spread: node.id
        max_replicas_per_node: 1
      restart_policy:
        condition: on-failure
        delay: 30s
        window: 30s
        max_attempts: 0 # unlimited restarts
      resources:
        limits:
          memory: "{{ elasticsearch_memory }}M"
        reservations:
          memory: "{{ elasticsearch_memory_reservation }}M"
{% if enable_elasticsearch_webui %}
      labels:
        caddy: "{{ elasticsearch_domain }}"
        # enable access log
        caddy.log: ""

        # hsts headers (see https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Strict-Transport-Security)
        caddy.1_header.Strict-Transport-Security: "\"max-age=63072000; includeSubDomains; preload\""

        # security labels
        caddy.1_header.-Server: ""
        caddy.1_header.-X-Powered-By: ""
        caddy.1_header.Permissions-Policy: "\"accelerometer=(), camera=(), interest-cohort=(), geolocation=(), gyroscope=(), magnetometer=(), microphone=(), payment=(), usb=()\"" # see best practices https://github.com/w3c/webappsec-permissions-policy/blob/master/features.md, https://developer.mozilla.org/en-US/docs/Web/HTTP/Feature_Policy
        caddy.1_header.Referrer-Policy: "same-origin"
        caddy.1_header.X-Content-Type-Options: "nosniff"
        caddy.1_header.X-Frame-Options: "DENY"
        caddy.1_header.X-XSS-Protection: "\"1; mode=block\""
        caddy.1_header.Content-Security-Policy: "\"default-src 'self'; script-src 'self'; img-src 'self'; style-src 'self'; font-src 'self'; frame-ancestors 'self'; frame-src 'self'; object-src 'none';\"" # https://csp-evaluator.withgoogle.com/ content security policy

        # ip filter auth & reverse proxy conf
        caddy.2_@ipfilter.remote_ip: "{{ elastic_allowed_ips|join(' ') }} 127.0.0.1"

        # apply ip filter
        caddy.3_handle: "@ipfilter"
        # basic auth
        caddy.3_handle.1_basicauth: "/*" # route matcher to protect
{% for authuser in elastic_backend_users %}
{% set user_loc = authuser.split(':') %}
        caddy.3_handle.1_basicauth.{{ user_loc[0] }}: "{{ user_loc[1] | replace('$', '$$') }}" # because '$' is evaluated, remember to escape it
{% endfor %}
        # try to match filter for a reverse proxy to upstream (eventually add load balancing etc)
        caddy.3_handle.2_reverse_proxy: "{% raw %}{{upstreams 9200}}{% endraw %}"
        caddy.3_handle.2_reverse_proxy.lb_policy: "round_robin"
        caddy.3_handle.2_reverse_proxy.health_uri: "/"
        caddy.3_handle.2_reverse_proxy.health_interval: "5s"

        # or reject
        caddy.4_respond: "`Origin not allowed` 403"
{% endif %}
    networks:
      - stack
{% if enable_elasticsearch_webui %}
      - {{ caddy_network }}
{% endif %}
    volumes:
      - 'es_data:/usr/share/opensearch/data'
    healthcheck:
      test: curl --silent --output --fail http://localhost:9200 || exit 1
      interval: 30s
      timeout: 10s
      retries: 5
      start_period: 600s

  kibana:
    image: "opensearchproject/opensearch-dashboards:{{ opensearch_version }}"
    entrypoint: >
      sh -c '
      cp -f /tmp/readonly/opensearch_dashboards.yml /usr/share/opensearch-dashboards/config/opensearch_dashboards.yml &&
      /usr/share/opensearch-dashboards/opensearch-dashboards-docker-entrypoint.sh '
    configs:
      - source: opensearch_dashboards.yml
        target: /tmp/readonly/opensearch_dashboards.yml
    deploy:
      mode: replicated
      replicas: 1
      placement:
        constraints:
          - node.labels.kibana_host == true
        preferences:
          - spread: node.id
        max_replicas_per_node: 1
      restart_policy:
        condition: on-failure
        delay: 30s
        max_attempts: 0 # unlimited restarts
        window: 120s
      labels:
        caddy: "{{ kibana_domain }}"
        # enable access log
        caddy.log: ""

        # hsts headers (see https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Strict-Transport-Security)
        caddy.1_header.Strict-Transport-Security: "\"max-age=63072000; includeSubDomains; preload\""

        # security labels
        caddy.1_header.-Server: ""
        caddy.1_header.-X-Powered-By: ""
        caddy.1_header.Permissions-Policy: "\"accelerometer=(), camera=(), interest-cohort=(), geolocation=(), gyroscope=(), magnetometer=(), microphone=(), payment=(), usb=()\"" # see best practices https://github.com/w3c/webappsec-permissions-policy/blob/master/features.md, https://developer.mozilla.org/en-US/docs/Web/HTTP/Feature_Policy
        caddy.1_header.Referrer-Policy: "same-origin"
        caddy.1_header.X-Content-Type-Options: "nosniff"
        caddy.1_header.X-Frame-Options: "DENY"
        caddy.1_header.X-XSS-Protection: "\"1; mode=block\""
        caddy.1_header.Content-Security-Policy: "\"default-src 'self' data:; script-src 'self' 'unsafe-inline' 'unsafe-eval'; img-src 'self' data:; style-src 'self' 'unsafe-inline'; font-src 'self'; frame-ancestors 'self'; frame-src 'self'; object-src 'none';\"" # https://csp-evaluator.withgoogle.com/ content security policy

        # ip filter auth & reverse proxy conf
        caddy.2_@ipfilter.remote_ip: "{{ elastic_allowed_ips|join(' ') }} 127.0.0.1"

        # apply ip filter
        caddy.3_handle: "@ipfilter"
        # basic auth
        caddy.3_handle.1_basicauth: "/*" # route matcher to protect
{% for authuser in elastic_backend_users %}
{% set user_loc = authuser.split(':') %}
        caddy.3_handle.1_basicauth.{{ user_loc[0] }}: "{{ user_loc[1] | replace('$', '$$') }}" # because '$' is evaluated, remember to escape it
{% endfor %}
        # try to match filter for a reverse proxy to upstream (eventually add load balancing etc)
        caddy.3_handle.2_reverse_proxy: "{% raw %}{{upstreams 5601}}{% endraw %}"
        caddy.3_handle.2_reverse_proxy.lb_policy: "round_robin"
        caddy.3_handle.2_reverse_proxy.health_uri: "/"
        caddy.3_handle.2_reverse_proxy.health_interval: "5s"

        # or reject
        caddy.4_respond: "`Origin not allowed` 403"
    environment:
      - "cluster.name=${ELASTIC_CLUSTER_NAME}"
      - "DISABLE_INSTALL_DEMO_CONFIG=true"
      - "DISABLE_SECURITY_DASHBOARDS_PLUGIN=true"
    networks:
      - stack
      - {{ caddy_network }}
    healthcheck:
      test: "curl --silent --output --fail http://localhost:5601 || exit 1"
      interval: 30s
      timeout: 30s
      retries: 40
      start_period: 1200s

{% if enable_webhook_smtp_forwarder %}
  # forward kibana alerts using smtp
  alerts-smtp-forwarder:
    image: "youtous/odfe-alerts-handler:latest"
    networks:
      - stack
    environment:
      - "SMTP_HOSTNAME=${SMTP_HOSTNAME}"
      - "SMTP_PORT=${SMTP_PORT}"
      - "SMTP_USERNAME=${SMTP_USERNAME}"
      - "SMTP_PASSWORD=${SMTP_PASSWORD}"
      - "SMTP_FROM=${SMTP_FROM}"
    healthcheck:
      interval: 30s
      timeout: 10s
      retries: 5
    deploy:
      mode: replicated
      replicas: 1
      restart_policy:
        condition: on-failure
        delay: 5s
        window: 30s
        max_attempts: 0 # unlimited restarts
{% endif %}

  # process logs from everywhere
  logstash:
    image: "opensearchproject/logstash-oss-with-opensearch-output-plugin:{{ logstash_elastic_version }}"
    configs:
      - source: logstash.yml
        target: /usr/share/logstash/config/logstash.yml
      - source: pipelines.yml
        target: /usr/share/logstash/config/pipelines.yml
{% for pipeline in logstash_pipeline_files %}
      - source: "pipeline_{{ pipeline }}"
        target: "/usr/share/logstash/pipeline-main/{{ pipeline }}"
{% endfor %}
{% for grok in logstash_pipeline_patterns %}
      - source: "pattern_{{ grok }}"
        target: "/usr/share/logstash/pipeline-main/patterns.d/{{ grok }}"
{% endfor %}
    secrets:
      - source: logstash-rootCA.crt
        target: /etc/ca.crt
      - source: logstash-certificate.crt
        target: /etc/server.crt
      - source: logstash-private-key.key
        target: /etc/server.key
    environment:
      - "LS_JAVA_OPTS=-Xms{{ logstash_memory }}m -Xmx{{ logstash_memory }}m -Djdk.tls.disabledAlgorithms=\"{{ elastic_disabled_tls_protocols|join(',') }}\""
      - "ELASTIC_CLUSTER_NAME=${ELASTIC_CLUSTER_NAME}"
    deploy:
      mode: replicated
      replicas: 1
      placement:
        constraints:
          - node.labels.logstash_host == true
      restart_policy:
        condition: on-failure
        delay: 5s
        window: 30s
        max_attempts: 0 # unlimited restarts
      resources:
        limits:
          memory: "{{ logstash_memory }}M"
        reservations:
          memory: "{{ logstash_memory_reservation }}M"
      labels:
        # traefik (tcp)
        traefik.enable: "true"
        traefik.docker.network: "{{ traefik_public_network }}"

        
        traefik.tcp.routers.logstash-5000.entrypoints: "logstash5000"

        traefik.tcp.routers.logstash-5000.rule: "HostSNI(`*`)"
        traefik.tcp.routers.logstash-5000.tls.passthrough: "true"
        traefik.tcp.routers.logstash-5000.service: "logstash-5000-proxy"
        traefik.tcp.routers.logstash-5000.middlewares: "logstash-ipwhitelist@docker"
        
        traefik.tcp.services.logstash-5000-proxy.loadbalancer.server.port: "5000"
        traefik.tcp.services.logstash-5000-proxy.loadbalancer.terminationDelay: "60000"


        traefik.tcp.routers.logstash-5044.entrypoints: "logstash5044"

        traefik.tcp.routers.logstash-5044.rule: "HostSNI(`*`)"
        traefik.tcp.routers.logstash-5044.tls.passthrough: "true"
        traefik.tcp.routers.logstash-5044.service: "logstash-5044-proxy"
        traefik.tcp.routers.logstash-5044.middlewares: "logstash-ipwhitelist@docker"
        
        traefik.tcp.services.logstash-5044-proxy.loadbalancer.server.port: "5044"
        traefik.tcp.services.logstash-5044-proxy.loadbalancer.terminationDelay: "60000"


        traefik.tcp.routers.logstash-5064.entrypoints: "logstash5064"

        traefik.tcp.routers.logstash-5064.rule: "HostSNI(`*`)"
        traefik.tcp.routers.logstash-5064.tls.passthrough: "true"
        traefik.tcp.routers.logstash-5064.service: "logstash-5064-proxy"
        traefik.tcp.routers.logstash-5064.middlewares: "logstash-external-ipwhitelist@docker"
        
        traefik.tcp.services.logstash-5064-proxy.loadbalancer.server.port: "5064"
        traefik.tcp.services.logstash-5064-proxy.loadbalancer.terminationDelay: "60000"
        
        traefik.tcp.middlewares.logstash-ipwhitelist.ipwhitelist.sourcerange: "{{ logstash_allowed_ips|join(', ') }}, 127.0.0.1/32" 
        traefik.tcp.middlewares.logstash-external-ipwhitelist.ipwhitelist.sourcerange: "{{ logstash_external_allowed_ips|join(', ') }}, 127.0.0.1/32"

        # todo : eventually enable TCP Keepalive when supported by traefik https://github.com/traefik/traefik/issues/1046

# TODO: enable it when supported, actually not yet supported https://github.com/traefik/traefik/issues/5598
#    
#         traefik.tcp.services.logstashproxy.loadbalancer.healthCheck.path: "/health?ready=1"
#         traefik.tcp.services.logstashproxy.loadbalancer.healthCheck.scheme: "http"
#         traefik.tcp.services.logstashproxy.loadbalancer.healthCheck.port: "8080"
#         traefik.tcp.services.logstashproxy.loadbalancer.healthCheck.interval: "10s"
#         traefik.tcp.services.logstashproxy.loadbalancer.healthCheck.timeout: "3s"
# 

{% if enable_logstash_webui %}
        caddy: "{{ logstash_domain }}"
        # enable access log
        caddy.log: ""

        # hsts headers (see https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Strict-Transport-Security)
        caddy.1_header.Strict-Transport-Security: "\"max-age=63072000; includeSubDomains; preload\""

        # security labels
        caddy.1_header.-Server: ""
        caddy.1_header.-X-Powered-By: ""
        caddy.1_header.Permissions-Policy: "\"accelerometer=(), camera=(), interest-cohort=(), geolocation=(), gyroscope=(), magnetometer=(), microphone=(), payment=(), usb=()\"" # see best practices https://github.com/w3c/webappsec-permissions-policy/blob/master/features.md, https://developer.mozilla.org/en-US/docs/Web/HTTP/Feature_Policy
        caddy.1_header.Referrer-Policy: "same-origin"
        caddy.1_header.X-Content-Type-Options: "nosniff"
        caddy.1_header.X-Frame-Options: "DENY"
        caddy.1_header.X-XSS-Protection: "\"1; mode=block\""
        caddy.1_header.Content-Security-Policy: "\"default-src 'self'; script-src 'self'; img-src 'self'; style-src 'self'; font-src 'self'; frame-ancestors 'self'; frame-src 'self'; object-src 'none';\"" # https://csp-evaluator.withgoogle.com/ content security policy

        # ip filter auth & reverse proxy conf
        caddy.2_@ipfilter.remote_ip: "{{ elastic_allowed_ips|join(' ') }} 127.0.0.1"

        # apply ip filter
        caddy.3_handle: "@ipfilter"
        # basic auth
        caddy.3_handle.1_basicauth: "/*" # route matcher to protect
{% for authuser in elastic_backend_users %}
{% set user_loc = authuser.split(':') %}
        caddy.3_handle.1_basicauth.{{ user_loc[0] }}: "{{ user_loc[1] | replace('$', '$$') }}" # because '$' is evaluated, remember to escape it
{% endfor %}
        # try to match filter for a reverse proxy to upstream (eventually add load balancing etc)
        caddy.3_handle.2_reverse_proxy: "{% raw %}{{upstreams 8080}}{% endraw %}"
        caddy.3_handle.2_reverse_proxy.lb_policy: "round_robin"
        caddy.3_handle.2_reverse_proxy.health_uri: "/"
        caddy.3_handle.2_reverse_proxy.health_interval: "5s"

        # or reject
        caddy.4_respond: "`Origin not allowed` 403"
{% endif %}
    networks:
      - stack
      - {{ traefik_public_network }}
{% if enable_logstash_webui %}
      - {{ caddy_network }}
{% endif %}
    healthcheck:
      test: curl --silent --output --fail http://localhost:9600 || exit 1
      interval: 30s
      timeout: 10s
      retries: 5
      start_period: 600s

  journalbeat-setup:
    image: "docker.elastic.co/beats/journalbeat-oss:{{ elastic_version }}" # only template due to incompatibility of kibana and beats agents (Elastic Licence)
    entrypoint: >
      sh -c '
      journalbeat setup --template
      '
    configs:
      - source: journalbeat.yml
        target: /usr/share/journalbeat/journalbeat.yml
    deploy:
      mode: replicated
      replicas: 1
      placement:
        constraints:
          - node.labels.elastic_host == true
      restart_policy:
        condition: on-failure
        delay: 30s
        window: 300s
        max_attempts: 0 # wait elastic and kibana to be up
    networks:
      - stack

  metricbeat-setup:
    image: "docker.elastic.co/beats/metricbeat-oss:{{ elastic_version }}" # only template due to incompatibility of kibana and beats agents (Elastic Licence)
    entrypoint: >
      sh -c '
      metricbeat setup --template
      '
    configs:
      - source: metricbeat.yml
        target: /usr/share/metricbeat/metricbeat.yml
    deploy:
      mode: replicated
      replicas: 1
      placement:
        constraints:
          - node.labels.elastic_host == true
      restart_policy:
        condition: on-failure
        delay: 30s
        window: 300s
        max_attempts: 0 # wait elastic and kibana to be up
    networks:
      - stack

  filebeat-setup:
    image: "docker.elastic.co/beats/filebeat-oss:{{ elastic_version }}" # only template due to incompatibility of kibana and beats agents (Elastic Licence)
    entrypoint: >
      sh -c '
      filebeat setup --template
      '
    configs:
      - source: filebeat.yml
        target: /usr/share/filebeat/filebeat.yml
    deploy:
      mode: replicated
      replicas: 1
      placement:
        constraints:
          - node.labels.elastic_host == true
      restart_policy:
        condition: on-failure
        delay: 30s
        window: 300s
        max_attempts: 0 # wait elastic and kibana to be up
    networks:
      - stack

  # check if services are up
  heartbeat:
    image: "docker.elastic.co/beats/heartbeat-oss:{{ elastic_version }}"
    entrypoint: >
      sh -c '
      heartbeat setup &&
      heartbeat --strict.perms=false -e
      '
    configs: # -e flag to log to stderr and disable syslog/file output
      - source: heartbeat.yml
        target: /usr/share/heartbeat/heartbeat.yml
    deploy:
      mode: replicated
      replicas: 1
      placement:
        constraints:
          - node.labels.elastic_host == true
      restart_policy:
        condition: on-failure
        delay: 30s
        window: 300s
        max_attempts: 0
      resources:
        limits:
          memory: 64M
    networks:
      - stack
    healthcheck:
      test: heartbeat test config
      interval: 30s
      timeout: 15s
      retries: 5
      start_period: 60s

  # backup opendistro indices
  elasticdump-opendistro:
    image: "elasticdump/elasticsearch-dump:latest"
    entrypoint: >
      sh -c '
      rm -Rf /tmp/older &&
      mv /tmp/current /tmp/older ;
      mkdir -p /tmp/current &&
      multielasticdump \
        --direction=dump \
        --match='^.opendistro-' \
        --input=http://elasticsearch:9200 \
        --output=/tmp/current
      '
    deploy:
      mode: replicated
      replicas: 0
      placement:
        constraints:
          - node.labels.elastic_host == true
      restart_policy:
        condition: none
      labels:
        - "swarm.cronjob.enable=true"
        - "swarm.cronjob.schedule=5 2 * * *"
        - "swarm.cronjob.skip-running=true" # do not start a job is one already exists
    networks:
      - stack
    volumes:
      - 'es_backup_opendistro:/tmp'
  # backup indices
  elasticdump:
    image: "elasticdump/elasticsearch-dump:latest"
    entrypoint: >
      sh -c '
      rm -Rf /tmp/older &&
      mv /tmp/current /tmp/older ;
      mkdir -p /tmp/current &&
      multielasticdump \
        --direction=dump \
        --match='^.kibana' \
        --input=http://elasticsearch:9200 \
        --output=/tmp/current
      '
    deploy:
      mode: replicated
      replicas: 0
      placement:
        constraints:
          - node.labels.elastic_host == true
      restart_policy:
        condition: none
      labels:
        - "swarm.cronjob.enable=true"
        - "swarm.cronjob.schedule=*/30 * * * *"
        - "swarm.cronjob.skip-running=true" # do not start a job is one already exists
    networks:
      - stack
    volumes:
      - 'es_backup:/tmp'
  # backup indices
  elasticdump-import:
    image: "elasticdump/elasticsearch-dump:latest"
    command: >
      --output=http://elasticsearch:9200/.kibana
      --input=/tmp/current/kibana_index.ndjson
      --type=data
    deploy:
      mode: replicated
      replicas: 0
      placement:
        constraints:
          - node.labels.elastic_host == true
      restart_policy:
        condition: none
    networks:
      - stack
    volumes:
      - 'es_backup:/tmp'

  # cleanup indices
  curator:
    image: "bobrik/curator:latest"
    command: "/actions.yml"
    configs:
      - source: actions.yml
        target: /actions.yml
      - source: curator.yml
        target: /.curator/curator.yml
    deploy:
      mode: replicated
      replicas: 0
      placement:
        constraints:
          - node.labels.elastic_host == true
      restart_policy:
        condition: none
      labels:
        - "swarm.cronjob.enable=true"
        - "swarm.cronjob.schedule=*/30 * * * *"
        - "swarm.cronjob.skip-running=true" # do not start a job is one already exists
    networks:
      - stack

networks:
  stack: # encrypted if Swarm want to split services on managers
    driver: overlay
{% if elastic_internal_network_encryption_enabled %}
    driver_opts:
      encrypted: ""
{% endif %}
  {{ caddy_network }}:
    external: true
  {{ traefik_public_network }}:
    external: true

# use docker volume to persist ES data outside of a container.
volumes:
  es_data: {}
  # backup kibana indices
  es_backup: {}
  es_backup_opendistro: {}

# documentation about keystore https://nicklang.com/posts/learning-to-love-the-keystore
# an other way to store sensible information but in this case, the user of the docker-cluster already have privileged access
configs:
  logstash.yml:
    name: "{{ docker_elastic_stack_name }}_logstash.j2.yml-${DEPLOY_TIMESTAMP}"
    file: ./config/logstash/logstash.j2.yml
  pipelines.yml:
    name: "{{ docker_elastic_stack_name }}_pipelines.j2.yml-${DEPLOY_TIMESTAMP}"
    file: ./config/logstash/pipelines.j2.yml
  opensearch.yml:
    name: "{{ docker_elastic_stack_name }}_opensearch.j2.yml-${DEPLOY_TIMESTAMP}"
    file: ./config/opensearch/opensearch.j2.yml
  opensearch_dashboards.yml:
    name: "{{ docker_elastic_stack_name }}_opensearch_dashboards.j2.yml-${DEPLOY_TIMESTAMP}"
    file: ./config/opensearch_dashboards/opensearch_dashboards.j2.yml
  heartbeat.yml:
    name: "{{ docker_elastic_stack_name }}_heartbeat.j2.yml-${DEPLOY_TIMESTAMP}"
    file: ./config/heartbeat/heartbeat.j2.yml
  filebeat.yml:
    name: "{{ docker_elastic_stack_name }}_filebeat.j2.yml-${DEPLOY_TIMESTAMP}"
    file: ./config/filebeat/filebeat.j2.yml
  journalbeat.yml:
    name: "{{ docker_elastic_stack_name }}_journalbeat.j2.yml-${DEPLOY_TIMESTAMP}"
    file: ./config/journalbeat/journalbeat.j2.yml
  metricbeat.yml:
    name: "{{ docker_elastic_stack_name }}_metricbeat.j2.yml-${DEPLOY_TIMESTAMP}"
    file: ./config/metricbeat/metricbeat.j2.yml
  actions.yml:
    name: "{{ docker_elastic_stack_name }}_actions.j2.yml-${DEPLOY_TIMESTAMP}"
    file: ./config/curator/actions.j2.yml
  curator.yml:
    name: "{{ docker_elastic_stack_name }}_curator.j2.yml-${DEPLOY_TIMESTAMP}"
    file: ./config/curator/curator.j2.yml

{% for pipeline in logstash_pipeline_files %}
  pipeline_{{ pipeline }}:
    name: "{{ docker_elastic_stack_name }}_{{ pipeline }}-${DEPLOY_TIMESTAMP}"
    file: "./config/logstash/pipeline/{{ pipeline }}"
{% endfor %}
{% for grok in logstash_pipeline_patterns %}
  pattern_{{ grok }}:
    name: "{{ docker_elastic_stack_name }}_{{ grok }}-${DEPLOY_TIMESTAMP}"
    file: "./config/logstash/pipeline/patterns.d/{{ grok }}"
{% endfor %}

secrets:
  logstash-rootCA.crt:
    name: "{{ docker_elastic_stack_name }}_logstash-rootCA.crt-${DEPLOY_TIMESTAMP}"
    external: true
  logstash-certificate.crt:
    name: "{{ docker_elastic_stack_name }}_logstash-certificate.crt-${DEPLOY_TIMESTAMP}"
    external: true
  logstash-private-key.key:
    name: "{{ docker_elastic_stack_name }}_logstash-private-key.key-${DEPLOY_TIMESTAMP}"
    external: true
