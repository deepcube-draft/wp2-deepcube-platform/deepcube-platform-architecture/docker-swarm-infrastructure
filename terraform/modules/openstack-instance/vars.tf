variable "instance_defaults" {
  type = any
  default = {}
}

variable "instance_spec" {
  type = any
  default = [ {} ]
}

variable "keypair_name" {
  type = string
}
